const instance = {
  name: "ETAS",
  policy_url: "https://www.etas.com/en/company/terms_of_use.php",
  partners: [
    {
      category: "Industry Partners",
      items: [
        {
          name: "Bosch",
          img: "https://bewebstudio.digitalauto.tech/data/projects/OezCm7PTy8FT/a/bosch.png",
          url: "https://www.bosch.com/",
        },
      ],
    },
    {
      category: "Standards & Open Source",
      items: [
        {
          name: "COVESA",
          img: "https://digitalauto.netlify.app/assets/COVESA-b3f64c5b.png",
          url: "https://www.covesa.global",
        },
        {
          name: "Eclipse Foundation",
          img: "https://www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-foundation-grey-orange.svg",
          url: "https://www.eclipse.org",
        },
      ],
    },
    {
      category: "Academic Partners",
      items: [
        {
          name: "Ferdinand-Steinbeis-Institut",
          img: "https://digitalauto.netlify.app/assets/FSTI-55cf60eb.png",
          url: "https://ferdinand-steinbeis-institut.de",
        },
      ],
    },
  ],
  text: {
    home_ads_pan_title: "Welcome to COVESA playground for virtual exploration",
    home_ads_pan_desc: `To support shift-level testing for software-defined vehicle (SDV) applications, we have created the digital.auto playground. This is a cloud-based, rapid prototyping environment for new, <b>SDV-enabled features</b>. The prototypes are built against real-world vehicle APIs and can be seamlessly migrated to automotive runtimes, such as Eclipse Velocitas. The playground is open and free to use.`,
  },
};

export default instance;
